package com.solidleon.spriteling.drawing;

import java.awt.*;

// --------------------------------------------------------------------------------
// spriteling
// File: ColorPalette.java
// Author: markusmannel
// Date: 10-24-2018
//
// Copyright 2018 Markus Mannel
// --------------------------------------------------------------------------------
public class ColorPalette
{

    public Color[] colors = new Color[16];

    public ColorPalette( Color startColor )
    {
        for( int i = 0; i < colors.length; i++ )
        {
            int r = startColor.getRed() + i * 15;
            int g = startColor.getGreen() + i  * 15;
            int b = startColor.getBlue() + i * 15;
            int a = startColor.getAlpha() + i * 15;

            colors[i] = new Color( clamp(r), clamp(g), clamp(b), clamp( a ) );
        }

    }

    private int clamp( int i )
    {
        return i > 255 ? 255 : i < 0 ? 0 : i;
    }
}
