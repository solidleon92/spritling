package com.solidleon.spriteling.drawing;

import java.awt.*;

// --------------------------------------------------------------------------------
// spriteling
// File: Sprite.java
// Author: markusmannel
// Date: 10-24-2018
//
// Copyright 2018 Markus Mannel
// --------------------------------------------------------------------------------
public class Sprite
{

    private int     width;
    private int     height;
    private Color[] pixels;
    private Color   outsideColor = new Color( 0, 0, 0, 0 );

    public Sprite( int width, int height )
    {
        this( width, height, new Color[width * height] );
    }

    public Sprite( int width, int height, Color[] pixels )
    {
        this.width = width;
        this.height = height;
        this.pixels = pixels;
    }

    public int getWidth()
    {
        return width;
    }

    public int getHeight()
    {
        return height;
    }

    public Color getColor( int x, int y )
    {
        if( !isValid( x, y ) )
            return outsideColor;
        return pixels[x + y * width];
    }

    public boolean isValid( int x, int y )
    {
        return x >= 0 && x < width && y >= 0 && y < height;
    }


    public void set( int x, int y, Color color )
    {
        if (isValid( x, y ))
            pixels[x + y * width ] = color;
    }
}
