package com.solidleon.spriteling.drawing;

import com.solidleon.spriteling.tools.DrawTool;
import com.solidleon.spriteling.tools.PencilTool;

import java.awt.*;

// --------------------------------------------------------------------------------
// spriteling
// File: DrawSettings.java
// Author: markusmannel
// Date: 10-24-2018
//
// Copyright 2018 Markus Mannel
// --------------------------------------------------------------------------------
public class DrawSettings
{

    public DrawTool drawTool = new PencilTool();

    public Color   foreground = Color.red;
    public Picture picture    = new Picture( 16, 16 );

    public int selectedLayerIndex;

    public boolean drawTilablePreview = false;
    public boolean drawGridOverlay    = true;
    public boolean drawGridBG = true;

    public int brushSize = 1;

    public boolean isoRender;

    public int     spritesheetTileWidth = 32;
    public int     spritesheetTileHeight = 32;
    public boolean drawSpritesheetGrid;
    public int isoTileHeight = 64;

    public void increaseBrushSize()
    {
        if (brushSize < 256)
            brushSize *= 2;
    }

    public void decreaseBrushSize()
    {
        if (brushSize > 1)
            brushSize /= 2;
    }
}
