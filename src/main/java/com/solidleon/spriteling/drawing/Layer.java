package com.solidleon.spriteling.drawing;

// --------------------------------------------------------------------------------
// spriteling
// File: Layer.java
// Author: markusmannel
// Date: 10-24-2018
//
// Copyright 2018 Markus Mannel
// --------------------------------------------------------------------------------
public class Layer
{

    public String  name;
    public Sprite  sprite;
    public boolean visible = true;
    public int     alpha = 255;

    public Layer( String name, Sprite sprite )
    {
        this.name = name;
        this.sprite = sprite;
    }

    public void toggleVisibility()
    {
        this.visible = !visible;
    }

    @Override
    public String toString()
    {
        return name + " " + (visible ? "" : "HIDDEN");
    }

}
