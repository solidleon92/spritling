package com.solidleon.spriteling.drawing;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

// --------------------------------------------------------------------------------
// spriteling
// File: Picture.java
// Author: markusmannel
// Date: 10-24-2018
//
// Copyright 2018 Markus Mannel
// --------------------------------------------------------------------------------
public class Picture
{
    private final int         width;
    private final int         height;
    private       List<Layer> layers = new ArrayList<>();

    public Picture( int width, int height )
    {
        this.width = width;
        this.height = height;
        addLayer();
    }

    public void addLayer()
    {
        layers.add( new Layer( "Layer " + ( layers.size() + 1 ), new Sprite( width, height ) ) );
    }

    public int layers()
    {
        return layers.size();
    }

    public Layer getLayer( int index )
    {
        if( index >= layers.size() )
            index = layers.size() - 1;
        if( index < 0 )
            index = 0;
        return layers.get( index );
    }

    public int getWidth()
    {
        return width;
    }

    public int getHeight()
    {
        return height;
    }

    public boolean isValid( int x, int y )
    {
        return x >= 0 && x < width && y >= 0 && y < height;
    }

    public void renderTo( BufferedImage image )
    {

        Graphics2D g = image.createGraphics();

        for( int i = 0; i < layers.size(); i++ )
        {
            if( !layers.get( i ).visible )
                continue;

            Composite compositeRestore = g.getComposite();

            if (layers.get( i ).alpha != 255)
            {
                g.setComposite( AlphaComposite.getInstance( AlphaComposite.SRC_OVER, layers.get( i ).alpha / 255.0f ) );
            }

            renderLayerTo( g, i, 0, 0 );

            g.setComposite( compositeRestore );
        }


    }

    public void renderLayerTo( Graphics2D g, int layerIndex, int xo, int yo )
    {
        for( int x = 0; x < width; x++ )
        {
            for( int y = 0; y < height; y++ )
            {
                Color color = layers.get( layerIndex ).sprite.getColor( x, y );
                if( color != null )
                {
                    g.setColor( color );
                    g.fillRect( xo + x, yo + y, 1, 1 );
                }

            }
        }
    }
    public void readANIFrom( BufferedImage image )
    {
        int tiles = image.getWidth() / width;
        for (int i = 0; i < tiles; i++) {
            if (i != 0) addLayer();
            for( int x = 0; x < width; x++ )
            {
                for( int y = 0; y < height; y++ )
                {

                    int pixel = image.getRGB( width * i + x, y );

                    int a = ( pixel >> 24 ) & 0xFF;
                    int r = ( pixel >> 16 ) & 0xFF;
                    int g = ( pixel >> 8 ) & 0xFF;
                    int b = ( pixel ) & 0xFF;

                    if( a > 0 )
                        layers.get( i ).sprite.set( x, y, new Color( r, g, b, a ) );
                }
            }
        }
    }

    public void readFrom( BufferedImage image )
    {
        for( int x = 0; x < width; x++ )
        {
            for( int y = 0; y < height; y++ )
            {

                int pixel = image.getRGB( x, y );

                int a = ( pixel >> 24 ) & 0xFF;
                int r = ( pixel >> 16 ) & 0xFF;
                int g = ( pixel >> 8 ) & 0xFF;
                int b = ( pixel ) & 0xFF;

                if( a > 0 )
                    layers.get( 0 ).sprite.set( x, y, new Color( r, g, b, a ) );
            }
        }
    }

    public boolean removeLayer( int index )
    {
        if( layers() > 1 )
        {
            if( index >= 0 && index < layers() )
            {
                layers.remove( index );
                return true;
            }
        }

        return false;
    }
    public void duplicateLayer(int layer) {
        Layer sourceLayer = layers.get(layer);
        addLayer();
        Layer newLayer = layers.get(layers()-1);
        for (int x = 0; x < sourceLayer.sprite.getWidth(); x++) {
            for (int y = 0; y < sourceLayer.sprite.getHeight(); y++) {
                newLayer.sprite.set(x, y, sourceLayer.sprite.getColor(x, y));
            }
        }

    }
}
