package com.solidleon.spriteling.tools;

import com.solidleon.spriteling.action.ActionStack;
import com.solidleon.spriteling.drawing.DrawSettings;
import com.solidleon.spriteling.drawing.Sprite;

// --------------------------------------------------------------------------------
// spriteling
// File: DrawTool.java
// Author: markusmannel
// Date: 10-24-2018
//
// Copyright 2018 Markus Mannel
// --------------------------------------------------------------------------------
public interface DrawTool
{
    void leftMouse( int x, int y, Sprite sprite, DrawSettings drawSettings, ActionStack actionStack );
    void rightMouse( int x, int y, Sprite sprite, DrawSettings drawSettings, ActionStack actionStack );

    default void leftMouseReleased( ActionStack actionStack ) {}

    default void rightMouseReleased( ActionStack actionStack ) {}
}
