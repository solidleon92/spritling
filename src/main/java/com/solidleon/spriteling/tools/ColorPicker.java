package com.solidleon.spriteling.tools;

import com.solidleon.spriteling.action.ActionStack;
import com.solidleon.spriteling.drawing.DrawSettings;
import com.solidleon.spriteling.drawing.Sprite;

import java.awt.*;

// --------------------------------------------------------------------------------
// spriteling
// File: ColorPicker.java
// Author: markusmannel
// Date: 10-24-2018
//
// Copyright 2018 Markus Mannel
// --------------------------------------------------------------------------------
public class ColorPicker implements DrawTool
{
    @Override
    public void leftMouse( int x, int y, Sprite sprite, DrawSettings drawSettings, ActionStack actionStack )
    {
        for( int layers = drawSettings.picture.layers() - 1; layers >= 0; layers-- )
        {
            if (drawSettings.picture.getLayer( layers ).visible)
            {
                Color color = drawSettings.picture.getLayer( layers ).sprite.getColor( x, y );
                if (color != null)
                {
                    drawSettings.foreground = color;
                    return;
                }
            }
        }
    }

    @Override
    public void rightMouse( int x, int y, Sprite sprite, DrawSettings drawSettings, ActionStack actionStack )
    {

    }
}
