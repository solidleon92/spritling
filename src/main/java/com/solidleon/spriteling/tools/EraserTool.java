package com.solidleon.spriteling.tools;

import com.solidleon.spriteling.action.ActionStack;
import com.solidleon.spriteling.drawing.DrawSettings;
import com.solidleon.spriteling.drawing.Sprite;

// --------------------------------------------------------------------------------
// spriteling
// File: EraserTool.java
// Author: markusmannel
// Date: 10-24-2018
//
// Copyright 2018 Markus Mannel
// --------------------------------------------------------------------------------
public class EraserTool implements DrawTool
{
    @Override
    public void leftMouse( int x, int y, Sprite sprite, DrawSettings drawSettings, ActionStack actionStack )
    {
        sprite.set( x, y, null );
    }

    @Override
    public void rightMouse( int x, int y, Sprite sprite, DrawSettings drawSettings, ActionStack actionStack )
    {
        sprite.set( x, y, null );
    }
}
