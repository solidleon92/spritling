package com.solidleon.spriteling.tools;

import com.solidleon.spriteling.action.ActionStack;
import com.solidleon.spriteling.action.CompositeAction;
import com.solidleon.spriteling.action.DrawAction;
import com.solidleon.spriteling.drawing.DrawSettings;
import com.solidleon.spriteling.drawing.Sprite;

// --------------------------------------------------------------------------------
// spriteling
// File: PencilTool.java
// Author: markusmannel
// Date: 10-24-2018
//
// Copyright 2018 Markus Mannel
// --------------------------------------------------------------------------------
public class PencilTool implements DrawTool
{

    private CompositeAction action = null;

    @Override
    public void leftMouseReleased( ActionStack actionStack )
    {
        if (action == null)
            return;
        actionStack.push( action );
        action = null;
    }

    @Override
    public void rightMouseReleased( ActionStack actionStack )
    {
        if (action == null)
            return;
        actionStack.push( action );
        action = null;
    }

    @Override
    public void leftMouse( int x, int y, Sprite sprite, DrawSettings drawSettings, ActionStack actionStack )
    {
        if( action == null )
            action = new CompositeAction();
        if( drawSettings.picture.getLayer( drawSettings.selectedLayerIndex ).visible )
        {
            for( int i = 0; i < drawSettings.brushSize; i++ )
            {
                for( int j = 0; j < drawSettings.brushSize; j++ )
                {
                    action.add( new DrawAction( sprite, x + i, y + j, drawSettings.foreground, sprite.getColor( x + i, y + j ) ) );
                    sprite.set( x + i, y + j, drawSettings.foreground );
                }
            }

        }
    }

    @Override
    public void rightMouse( int x, int y, Sprite sprite, DrawSettings drawSettings, ActionStack actionStack )
    {
        if( action == null )
            action = new CompositeAction();
        if( drawSettings.picture.getLayer( drawSettings.selectedLayerIndex ).visible )
        {
            for( int i = 0; i < drawSettings.brushSize; i++ )
            {
                for( int j = 0; j < drawSettings.brushSize; j++ )
                {
                    action.add( new DrawAction( sprite, x + i, y + j, null, sprite.getColor( x + i, y + j ) ) );
                    sprite.set( x + i, y + j, null );
                }
            }

        }
    }
}
