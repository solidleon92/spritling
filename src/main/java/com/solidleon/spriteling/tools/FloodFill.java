package com.solidleon.spriteling.tools;

import com.solidleon.spriteling.action.ActionStack;
import com.solidleon.spriteling.action.CompositeAction;
import com.solidleon.spriteling.action.DrawAction;
import com.solidleon.spriteling.drawing.DrawSettings;
import com.solidleon.spriteling.drawing.Sprite;

import java.awt.*;

// --------------------------------------------------------------------------------
// spriteling
// File: FloodFill.java
// Author: markusmannel
// Date: 10-24-2018
//
// Copyright 2018 Markus Mannel
// --------------------------------------------------------------------------------
public class FloodFill implements DrawTool
{
    @Override
    public void leftMouse( int x, int y, Sprite sprite, DrawSettings drawSettings, ActionStack actionStack )
    {
        if (colorMatch( sprite.getColor( x, y ), drawSettings.foreground ))
            return;

        CompositeAction action = new CompositeAction();
        fill4(sprite, x, y, sprite.getColor( x, y ), drawSettings.foreground, action);

        actionStack.push( action );
    }

    private void fill4( Sprite sprite, int x, int y, Color sourceColor, Color foreground, CompositeAction action )
    {
        if (!sprite.isValid( x, y ))
            return;
        if (colorMatch(sourceColor, sprite.getColor( x, y )))
        {
            action.add( new DrawAction( sprite, x, y, foreground, sprite.getColor( x, y ) ) );
            sprite.set( x, y, foreground );

            fill4( sprite, x, y + 1, sourceColor, foreground, action );
            fill4( sprite, x, y - 1, sourceColor, foreground, action );
            fill4( sprite, x-1, y , sourceColor, foreground, action );
            fill4( sprite, x+1, y, sourceColor, foreground, action );
        }
    }

    private boolean colorMatch( Color a, Color b )
    {
        if (a == null && b == null)
            return true;
        if (a != null && b != null)
            return a.equals( b );
        return false;
    }

    @Override
    public void rightMouse( int x, int y, Sprite sprite, DrawSettings drawSettings, ActionStack actionStack )
    {

        if (colorMatch( sprite.getColor( x, y ), null))
            return;
        CompositeAction action = new CompositeAction();
        fill4(sprite, x, y, sprite.getColor( x, y ), null, action );
        actionStack.push( action );
    }
}
