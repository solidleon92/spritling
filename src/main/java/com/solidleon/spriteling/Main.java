package com.solidleon.spriteling;

// --------------------------------------------------------------------------------
// spriteling
// File: Main.java
// Author: markusmannel
// Date: 10-24-2018
//
// Copyright 2018 Markus Mannel
// --------------------------------------------------------------------------------
public class Main
{
    public static void main( String[] args )
    {
        MainWindow mainWindow = new MainWindow();

        mainWindow.open();

    }
}
