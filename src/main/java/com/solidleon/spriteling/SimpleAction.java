package com.solidleon.spriteling;

import javax.swing.*;
import java.awt.event.ActionEvent;

// --------------------------------------------------------------------------------
// spriteling
// File: SimpleAction.java
// Author: markusmannel
// Date: 10-24-2018
//
// Copyright 2018 Markus Mannel
// --------------------------------------------------------------------------------
public class SimpleAction extends AbstractAction
{

    private Runnable action;

    public SimpleAction( String name, Runnable action)
    {
        super(name);
        this.action = action;
    }
    public SimpleAction( String name, Runnable action, KeyStroke shortcut)
    {
        super(name);
        putValue(Action.ACCELERATOR_KEY, shortcut);
        this.action = action;
    }

    public SimpleAction( String iconResource, String name, Runnable action )
    {
        super(name, new ImageIcon( SimpleAction.class.getResource( iconResource )));
        this.action = action;
    }
    public SimpleAction( String iconResource, String name, Runnable action, KeyStroke shortcut )
    {
        super(name, new ImageIcon( SimpleAction.class.getResource( iconResource )));
        putValue(Action.ACCELERATOR_KEY, shortcut);
        this.action = action;
    }

    @Override
    public void actionPerformed( ActionEvent e )
    {
        if (action != null)
            action.run();
    }
}
