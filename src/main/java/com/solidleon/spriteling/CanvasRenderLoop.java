package com.solidleon.spriteling;

import com.solidleon.spriteling.action.ActionStack;
import com.solidleon.spriteling.drawing.DrawSettings;
import com.solidleon.spriteling.drawing.Layer;
import com.solidleon.spriteling.drawing.Sprite;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.image.*;

// --------------------------------------------------------------------------------
// spriteling
// File: CanvasRenderLoop.java
// Author: markusmannel
// Date: 10-24-2018
//
// Copyright 2018 Markus Mannel
// --------------------------------------------------------------------------------
public class CanvasRenderLoop
{
    private MainWindow   mainWindow;
    private Canvas       canvas;
    private DrawSettings drawSettings;
    private ActionStack  actionStack;


    private boolean stopped;
    private boolean leftMoueDown;
    private boolean rightMoueDown;
    private boolean middleMoueDown;
    private int     mx, my;
    private int xo, yo;
    private int pixelSize = 32;


    public CanvasRenderLoop( MainWindow mainWindow, Canvas canvas, DrawSettings drawSettings, ActionStack actionStack )
    {
        this.mainWindow = mainWindow;
        this.canvas = canvas;
        this.drawSettings = drawSettings;
        this.actionStack = actionStack;
    }

    public void start()
    {
        new Thread( this::loop ).start();
    }

    private void loop()
    {
        init();
        while( !stopped )
        {

            update();
            render();

            try
            {
                Thread.sleep( 10 );
            }
            catch( InterruptedException e )
            {
                e.printStackTrace();
            }
        }

    }

    private void update()
    {

    }

    private void init()
    {
        canvas.addMouseMotionListener( new MouseAdapter()
        {
            @Override
            public void mouseDragged( MouseEvent e )
            {
                int oldx = mx;
                int oldy = my;
                mx = e.getX();
                my = e.getY();

                if( middleMoueDown )
                {
                    int dx = mx - oldx;
                    int dy = my - oldy;
                    xo += dx;
                    yo += dy;
                }

                int xto = (int) ( ( oldx - xo ) / ( pixelSizeD() * drawSettings.brushSize ) ) * drawSettings.brushSize;
                int yto = (int) ( ( oldy - yo ) / ( pixelSizeD() * drawSettings.brushSize ) ) * drawSettings.brushSize;

                int xt = (int) ( ( mx - xo ) / ( pixelSizeD() * drawSettings.brushSize ) ) * drawSettings.brushSize;
                int yt = (int) ( ( my - yo ) / ( pixelSizeD() * drawSettings.brushSize ) ) * drawSettings.brushSize;

                if( xt == xto && yt == yto )
                    return;

                mainWindow.setDrawCursorPosition( xt, yt );

                if( leftMoueDown )
                {
                    drawSettings.drawTool.leftMouse( xt, yt, drawSettings.picture.getLayer( drawSettings.selectedLayerIndex ).sprite, drawSettings, actionStack );
                }
                if( rightMoueDown )
                {
                    drawSettings.drawTool.rightMouse( xt, yt, drawSettings.picture.getLayer( drawSettings.selectedLayerIndex ).sprite, drawSettings, actionStack );
                }
            }

            @Override
            public void mouseMoved( MouseEvent e )
            {
                mx = e.getX();
                my = e.getY();
                int xt = (int) ( ( mx - xo ) / ( pixelSizeD() * drawSettings.brushSize ) ) * drawSettings.brushSize;
                int yt = (int) ( ( my - yo ) / ( pixelSizeD() * drawSettings.brushSize ) ) * drawSettings.brushSize;

                mainWindow.setDrawCursorPosition( xt, yt );
            }
        } );
        canvas.addMouseListener( new MouseAdapter()
        {
            @Override
            public void mousePressed( MouseEvent e )
            {
                if( e.getButton() == MouseEvent.BUTTON1 )
                    leftMoueDown = true;
                else if( e.getButton() == MouseEvent.BUTTON2 )
                    middleMoueDown = true;
                else if( e.getButton() == MouseEvent.BUTTON3 )
                    rightMoueDown = true;

                int xt = (int) ( ( mx - xo ) / ( pixelSizeD() * drawSettings.brushSize ) ) * drawSettings.brushSize;
                int yt = (int) ( ( my - yo ) / ( pixelSizeD() * drawSettings.brushSize ) ) * drawSettings.brushSize;

                mainWindow.setDrawCursorPosition( xt, yt );

                if( leftMoueDown )
                {
                    drawSettings.drawTool.leftMouse( xt, yt, drawSettings.picture.getLayer( drawSettings.selectedLayerIndex ).sprite, drawSettings, actionStack );
                }
                if( rightMoueDown )
                {
                    drawSettings.drawTool.rightMouse( xt, yt, drawSettings.picture.getLayer( drawSettings.selectedLayerIndex ).sprite, drawSettings, actionStack );
                }
            }

            @Override
            public void mouseReleased( MouseEvent e )
            {
                if( e.getButton() == MouseEvent.BUTTON1 )
                {
                    leftMoueDown = false;
                    drawSettings.drawTool.leftMouseReleased( actionStack );
                }
                else if( e.getButton() == MouseEvent.BUTTON2 )
                    middleMoueDown = false;
                else if( e.getButton() == MouseEvent.BUTTON3 )
                {
                    rightMoueDown = false;
                    drawSettings.drawTool.rightMouseReleased( actionStack );
                }
            }
        } );


        canvas.addMouseWheelListener( new MouseAdapter()
        {
            @Override
            public void mouseWheelMoved( MouseWheelEvent e )
            {
                int change = e.getUnitsToScroll();
                if( change < 0 )
                {
                    // zoom in
                    if( pixelSize * 2 < 256 )
                    {
                        pixelSize *= 2;
                        xo -= pixelSize;
                        yo -= pixelSize;
                    }
                }
                else if( change > 0 )
                {
                    // zoom in
                    if( pixelSize / 2 >= 1 )
                    {
                        pixelSize /= 2;
                        xo += pixelSize;
                        yo += pixelSize;
                    }
                }
            }
        } );

        xo = ( canvas.getWidth() - drawSettings.picture.getWidth() * pixelSize ) / 2;
        yo = ( canvas.getHeight() - drawSettings.picture.getHeight() * pixelSize ) / 2;
    }

    private void render()
    {
        BufferStrategy bs = canvas.getBufferStrategy();
        if( bs == null )
        {
            canvas.createBufferStrategy( 2 );
            return;
        }

        Graphics2D g = (Graphics2D) bs.getDrawGraphics();

        g.clearRect( 0, 0, canvas.getWidth(), canvas.getHeight() );

        renderSprite( g );


        g.dispose();
        bs.show();
    }

    private void renderSprite( Graphics2D g )
    {
        g.translate( xo, yo );
        g.clearRect( 0, 0, drawSettings.picture.getWidth() * pixelSize, drawSettings.picture.getHeight() * pixelSize );

        if( drawSettings.drawGridBG )
        {
            for( int x = 0; x < drawSettings.picture.getWidth(); x++ )
            {
                for( int y = 0; y < drawSettings.picture.getHeight(); y++ )
                {
                    g.setColor( ( x + y ) % 2 == 0 ? Color.gray : Color.lightGray );
                    g.fillRect( x * pixelSize, y * pixelSize, pixelSize, pixelSize );

                }
            }
        }

        if( drawSettings.drawTilablePreview )
        {

            if( drawSettings.isoRender )
                renderTileableIso1_2( g );
            else
                renderTileable2D( g );

        }
        else
        {
            drawVisibleLayers( g, 0, 0 );
        }

        if( drawSettings.drawGridOverlay )
        {

            for( int x = 0; x < drawSettings.picture.getWidth() / drawSettings.brushSize; x++ )
            {
                for( int y = 0; y < drawSettings.picture.getHeight() / drawSettings.brushSize; y++ )
                {
                    g.setColor( Color.lightGray );
                    g.drawRect( x * pixelSize * drawSettings.brushSize, y * pixelSize * drawSettings.brushSize, pixelSize * drawSettings.brushSize, pixelSize * drawSettings.brushSize );

                }
            }
        }

        if( drawSettings.drawSpritesheetGrid )
        {
            int tilesX = drawSettings.picture.getWidth() / drawSettings.spritesheetTileWidth;
            int tilesY = drawSettings.picture.getHeight() / drawSettings.spritesheetTileHeight;

            for( int x = 0; x < tilesX; x++ )
            {
                for( int y = 0; y < tilesY; y++ )
                {
                    g.setColor( Color.cyan );
                    g.drawRect( x * drawSettings.spritesheetTileWidth * pixelSize, y * drawSettings.spritesheetTileHeight * pixelSize, drawSettings.spritesheetTileWidth * pixelSize, drawSettings.spritesheetTileHeight * pixelSize );

                }
            }
        }

        int xt = (int) ( ( mx - xo ) / ( pixelSizeD() * drawSettings.brushSize ) );
        int yt = (int) ( ( my - yo ) / ( pixelSizeD() * drawSettings.brushSize ) );
        if( drawSettings.picture.isValid( xt, yt ) )
        {
            g.setColor( Color.yellow );
            g.drawRect( xt * pixelSize * drawSettings.brushSize, yt * pixelSize * drawSettings.brushSize, pixelSize * drawSettings.brushSize, pixelSize * drawSettings.brushSize );
        }

    }

    private void renderTileable2D( Graphics2D g )
    {
        for( int i = 0; i < 3; i++ )
        {
            for( int j = 0; j < 3; j++ )
            {
                drawVisibleLayers( g, ( i - 1 ) * ( drawSettings.picture.getWidth() * pixelSize ), ( j - 1 ) * ( ( drawSettings.picture.getHeight() * pixelSize ) ) );
            }
        }
    }

    private void renderTileableIso1_2( Graphics2D g )
    {
        int tw = drawSettings.picture.getWidth() * pixelSize;
        int th = drawSettings.isoTileHeight * pixelSize;
        for( int i = 0; i < 3; i++ )
        {
            for( int j = 0; j < 3; j++ )
            {
                int xt = i - 1;
                int yt = j - 1;
                int x  = ( xt - yt ) * ( tw / 2 );
                int y  = ( xt + yt ) * ( th / 2 );


                drawVisibleLayers( g, x, y );
            }
        }
    }


    private void drawVisibleLayers( Graphics2D g2, int xo, int yo )
    {


        for( int layerIndex = 0; layerIndex < drawSettings.picture.layers(); layerIndex++ )
        {
            Layer layer = drawSettings.picture.getLayer( layerIndex );
            if( !layer.visible )
                continue;
            Sprite sprite = layer.sprite;


            Composite compositeRestore = g2.getComposite();
            if( layer.alpha != 255 )
            {
                g2.setComposite( AlphaComposite.getInstance( AlphaComposite.SRC_OVER, layer.alpha / 255.0f ) );
            }

            for( int x = 0; x < drawSettings.picture.getWidth(); x++ )
            {
                for( int y = 0; y < drawSettings.picture.getHeight(); y++ )
                {
                    Color color = sprite.getColor( x, y );
                    if( color != null )
                    {
                        g2.setColor( color );
                        g2.fillRect( xo + x * pixelSize, yo + y * pixelSize, pixelSize, pixelSize );
                    }
                }
            }

            g2.setComposite( compositeRestore );
        }

    }

    private double pixelSizeD()
    {
        return (double) pixelSize;
    }

    public void stop()
    {
        stopped = true;
    }
}
