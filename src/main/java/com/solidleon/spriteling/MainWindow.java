package com.solidleon.spriteling;

import com.solidleon.spriteling.action.ActionStack;
import com.solidleon.spriteling.drawing.DrawSettings;
import com.solidleon.spriteling.drawing.Layer;
import com.solidleon.spriteling.drawing.Picture;
import com.solidleon.spriteling.tools.ColorPicker;
import com.solidleon.spriteling.tools.EraserTool;
import com.solidleon.spriteling.tools.FloodFill;
import com.solidleon.spriteling.tools.PencilTool;
import com.solidleon.spriteling.ui.ColorChangeButton;
import com.solidleon.spriteling.ui.ColorManagerComponent;
import com.solidleon.spriteling.ui.JXToolBar;
import com.solidleon.spriteling.ui.LayerListModel;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

// --------------------------------------------------------------------------------
// spriteling
// File: MainWindow.java
// Author: markusmannel
// Date: 10-24-2018
//
// Copyright 2018 Markus Mannel
// --------------------------------------------------------------------------------
public class MainWindow {

    private final JLabel drawCursorPosition = new JLabel();
    private final JLabel pictureDimensions = new JLabel();
    private final JSlider layerAlphaSlider;
    private ActionStack actionStack = new ActionStack();
    private JFileChooser fileChooser = new JFileChooser(System.getProperty("user.dir"));

    private JFrame frame;
    private JToolBar toolBar = new JXToolBar();
    private Canvas canvas = new Canvas();
    private DrawSettings drawSettings = new DrawSettings();
    private JList<Layer> layers = new JList<>(new LayerListModel(drawSettings.picture));
    private CanvasRenderLoop renderLoop = new CanvasRenderLoop(this, canvas, drawSettings, actionStack);

    private JColorChooser colorChooser = new JColorChooser();


    public MainWindow() {
        frame = new JFrame("Spriteling - " + MainWindow.class.getPackage().getImplementationVersion());
        frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        frame.setPreferredSize(new Dimension(1280, 720));


        toolBar.setFloatable(false);

        toolBar.add(new SimpleAction("/icons/new.png", "new", this::newPicture, KeyStroke.getKeyStroke(KeyEvent.VK_N, KeyEvent.CTRL_DOWN_MASK)));
        toolBar.add(new SimpleAction("/icons/open.png", "Open", this::openFile, KeyStroke.getKeyStroke(KeyEvent.VK_O, KeyEvent.CTRL_DOWN_MASK)));
        toolBar.add(new SimpleAction("/icons/open-ani.png", "Open", this::openANI, KeyStroke.getKeyStroke(KeyEvent.VK_O, KeyEvent.CTRL_DOWN_MASK | KeyEvent.SHIFT_DOWN_MASK)));
        toolBar.addSeparator();
        toolBar.add(new SimpleAction("/icons/save.png", "Save", this::save));
        toolBar.add(new SimpleAction("/icons/export-png.png", "PNG", this::exportPNG));
        toolBar.add(new SimpleAction("/icons/export-layers.png", "PNG", this::exportLayers));
        toolBar.add(new SimpleAction("/icons/export-ani.png", "Ani", this::exportANI));
        toolBar.addSeparator();
        toolBar.add(new SimpleAction("/icons/undo.png", "Undo", () -> actionStack.undoLast()));
        toolBar.addSeparator();
        toolBar.add(new SimpleAction("/icons/pencil.png", "Pencil", () -> drawSettings.drawTool = new PencilTool(), KeyStroke.getKeyStroke('b')));
        toolBar.add(new SimpleAction("/icons/eraser.png", "Eraser", () -> drawSettings.drawTool = new EraserTool(), KeyStroke.getKeyStroke('e')));
        toolBar.add(new SimpleAction("/icons/picker.png", "Picker", () -> drawSettings.drawTool = new ColorPicker(), KeyStroke.getKeyStroke('p')));
        toolBar.add(new SimpleAction("/icons/fill.png", "Fill", () -> drawSettings.drawTool = new FloodFill(), KeyStroke.getKeyStroke('f')));
        toolBar.add(new ColorChangeButton("Color", () -> drawSettings.foreground, color -> drawSettings.foreground = color));
        toolBar.addSeparator();
        toolBar.add(new SimpleAction("/icons/iso.png", "ISO/Ortho", () -> drawSettings.isoRender = !drawSettings.isoRender));
        toolBar.add(new SimpleAction("Tilable", () -> drawSettings.drawTilablePreview = !drawSettings.drawTilablePreview));
        toolBar.add(new SimpleAction("Grid BG", () -> drawSettings.drawGridBG = !drawSettings.drawGridBG));
        toolBar.add(new SimpleAction("Grid FG", () -> drawSettings.drawGridOverlay = !drawSettings.drawGridOverlay));
        toolBar.add(new SimpleAction("Spritesheet", () -> drawSettings.drawSpritesheetGrid = !drawSettings.drawSpritesheetGrid));
        toolBar.add(new SimpleAction("Tile Size", () -> setTileSize()));
        toolBar.add(new SimpleAction("Brush+", () -> drawSettings.increaseBrushSize()));
        toolBar.add(new SimpleAction("Brush-", () -> drawSettings.decreaseBrushSize()));
        toolBar.addSeparator();

        frame.add(toolBar, BorderLayout.NORTH);


        colorChooser.getSelectionModel().addChangeListener(e -> drawSettings.foreground = colorChooser.getColor());

        JPanel layerPanel = new JPanel(new BorderLayout());
        JToolBar layerToolbar = new JXToolBar();
        layerToolbar.setFloatable(false);
        layerToolbar.add(new SimpleAction("+", this::addLayer));
        layerToolbar.add(new SimpleAction("-", this::removeLayer));
        layerToolbar.add(new SimpleAction("Dupe", this::duplicateLayer,KeyStroke.getKeyStroke('D', KeyEvent.CTRL_DOWN_MASK)));
        layerToolbar.add(new SimpleAction("Visibility", () ->
        {
            for (int selectedIndex : layers.getSelectedIndices()) {
                drawSettings.picture.getLayer(selectedIndex).toggleVisibility();
            }
            layers.repaint();
        }));
        layers.setSelectedIndex(0);
        layers.setCellRenderer(new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                JLabel label = (JLabel) super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                if (!((Layer) value).visible)
                    label.setForeground(Color.gray);
                return label;
            }
        });
        layerPanel.add(layerToolbar, BorderLayout.NORTH);
        JPanel layerCenter = new JPanel(new BorderLayout());
        JPanel layerSettings = new JPanel();
        layerSettings.add(new JLabel("Alpha:"));
        layerSettings.add(layerAlphaSlider = new JSlider(0, 255, 255));
        layerAlphaSlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                for (int selectedIndex : layers.getSelectedIndices()) {
                    drawSettings.picture.getLayer(selectedIndex).alpha = layerAlphaSlider.getValue();
                }
            }
        });
        layerCenter.add(layerSettings, BorderLayout.NORTH);
        layerCenter.add(new JScrollPane(layers), BorderLayout.CENTER);
        layerPanel.add(layerCenter, BorderLayout.CENTER);

        JPanel tools = new JPanel(new BorderLayout());
        tools.add(layerPanel, BorderLayout.CENTER);
        tools.add(new ColorManagerComponent(this, drawSettings), BorderLayout.SOUTH);

        JPanel statusBar = new JPanel(new FlowLayout(FlowLayout.LEFT));
        //statusBar.setPreferredSize( new Dimension( 0, 16 ) );
        statusBar.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));

        drawCursorPosition.setFont(new Font("Verdana", Font.PLAIN, 8));
        pictureDimensions.setFont(new Font("Verdana", Font.PLAIN, 8));
        statusBar.add(drawCursorPosition);
        statusBar.add(pictureDimensions);

        frame.add(canvas, BorderLayout.CENTER);
        frame.add(tools, BorderLayout.EAST);
        frame.add(statusBar, BorderLayout.SOUTH);


        frame.pack();
        frame.setLocationRelativeTo(null);

        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                closeGracefully();
            }
        });

        layers.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                int index = layers.getSelectedIndex();
                if (index >= 0 && index < drawSettings.picture.layers()) {
                    System.out.println("SELECT LAYER: " + index);
                    drawSettings.selectedLayerIndex = index;
                    layerAlphaSlider.setValue(drawSettings.picture.getLayer(drawSettings.selectedLayerIndex).alpha);
                }
            }
        });


    }

    private void setTileSize() {
        String tw = JOptionPane.showInputDialog("Tile Width:");
        if (tw == null)
            return;
        String th = JOptionPane.showInputDialog("Tile Width:");
        if (th == null)
            return;

        drawSettings.spritesheetTileWidth = Integer.parseInt(tw);
        drawSettings.spritesheetTileHeight = Integer.parseInt(th);

    }

    private void save() {

    }

    private void removeLayer() {
        if (drawSettings.picture.removeLayer(drawSettings.selectedLayerIndex)) {
            drawSettings.selectedLayerIndex--;
            layers.setSelectedIndex(drawSettings.selectedLayerIndex);
            ((LayerListModel) layers.getModel()).refresh(drawSettings);
        }
    }

    private void addLayer() {
        drawSettings.picture.addLayer();
        drawSettings.selectedLayerIndex = drawSettings.picture.layers() - 1;
        layers.setSelectedIndex(drawSettings.selectedLayerIndex);
        ((LayerListModel) layers.getModel()).refresh(drawSettings);
    }

    private void duplicateLayer() {
        drawSettings.picture.duplicateLayer(drawSettings.selectedLayerIndex);
        drawSettings.selectedLayerIndex = drawSettings.picture.layers() - 1;
        layers.setSelectedIndex(drawSettings.selectedLayerIndex);
        ((LayerListModel) layers.getModel()).refresh(drawSettings);
    }

    private void openFile() {

        if (JFileChooser.APPROVE_OPTION != fileChooser.showOpenDialog(frame))
            return;


        try {
            BufferedImage image = ImageIO.read(fileChooser.getSelectedFile());

            drawSettings.picture = new Picture(image.getWidth(), image.getHeight());
            drawSettings.picture.readFrom(image);


            pictureDimensions.setText(String.format("w: %d h: %d", drawSettings.picture.getWidth(), drawSettings.picture.getHeight()));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    private void openANI() {

        if (JFileChooser.APPROVE_OPTION != fileChooser.showOpenDialog(frame))
            return;

        String sw = JOptionPane.showInputDialog("Width:");
        if (sw == null)
            return;
        String sh = JOptionPane.showInputDialog("Height:");
        if (sh == null)
            return;

        int tw = Integer.parseInt(sw);
        int th = Integer.parseInt(sh);

        try {
            BufferedImage image = ImageIO.read(fileChooser.getSelectedFile());

            drawSettings.picture = new Picture(tw, th);
            drawSettings.picture.readANIFrom(image);

            pictureDimensions.setText(String.format("w: %d h: %d", drawSettings.picture.getWidth(), drawSettings.picture.getHeight()));
            while (layers.getModel().getSize() < drawSettings.picture.layers()) {
                ((LayerListModel) layers.getModel()).refresh(drawSettings);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void exportPNG() {

        if (JFileChooser.APPROVE_OPTION != fileChooser.showSaveDialog(frame))
            return;


        BufferedImage image = new BufferedImage(drawSettings.picture.getWidth(), drawSettings.picture.getHeight(), BufferedImage.TYPE_INT_ARGB);
        drawSettings.picture.renderTo(image);
        try {
            ImageIO.write(image, "PNG", fileChooser.getSelectedFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void exportANI() {

        if (JFileChooser.APPROVE_OPTION != fileChooser.showSaveDialog(frame))
            return;


        int width = 0;
        for (int i = 0; i < drawSettings.picture.layers(); i++) {
            Layer layer = drawSettings.picture.getLayer(i);
            if (!layer.visible)
                continue;
            width += drawSettings.picture.getWidth();
        }

        BufferedImage image = new BufferedImage(width, drawSettings.picture.getHeight(), BufferedImage.TYPE_INT_ARGB);

        Graphics2D g = image.createGraphics();

        int xo = 0;
        for (int i = 0; i < drawSettings.picture.layers(); i++) {
            Layer layer = drawSettings.picture.getLayer(i);
            if (!layer.visible)
                continue;
            drawSettings.picture.renderLayerTo(g, i, xo, 0);
            xo += drawSettings.picture.getWidth();
        }
        g.dispose();
        try {
            ImageIO.write(image, "PNG", fileChooser.getSelectedFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void exportLayers() {

        if (JFileChooser.APPROVE_OPTION != fileChooser.showSaveDialog(frame))
            return;

        int num = 0;
        for (int i = 0; i < drawSettings.picture.layers(); i++) {
            if (drawSettings.picture.getLayer(i).visible) {
                exportLayer(i, num);
                num++;
            }
        }

    }

    private void exportLayer(int layerIndex, int num) {
        BufferedImage image = new BufferedImage(drawSettings.picture.getWidth(), drawSettings.picture.getHeight(), BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = image.createGraphics();
        drawSettings.picture.renderLayerTo(g, layerIndex, 0, 0);
        g.dispose();
        try {
            ImageIO.write(image, "PNG", new File(fileChooser.getSelectedFile().getAbsolutePath() + "-" + num + ".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void closeGracefully() {
        renderLoop.stop();
        frame.dispose();
        System.exit(0);
    }

    public void open() {
        SwingUtilities.invokeLater(() -> frame.setVisible(true));

        renderLoop.start();
    }

    public void setDrawCursorPosition(int x, int y) {
        drawCursorPosition.setText(String.format("x: %d y: %d", x, y));
    }


    private void newPicture() {

        String sw = JOptionPane.showInputDialog("Width:");
        if (sw == null)
            return;
        String sh = JOptionPane.showInputDialog("Height:");
        if (sh == null)
            return;

        drawSettings.picture = new Picture(Integer.parseInt(sw), Integer.parseInt(sh));
        ((LayerListModel) layers.getModel()).refresh(drawSettings);
        actionStack.clear();
        pictureDimensions.setText(String.format("w: %d h: %d", drawSettings.picture.getWidth(), drawSettings.picture.getHeight()));
    }
}
