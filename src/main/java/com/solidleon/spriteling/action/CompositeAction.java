package com.solidleon.spriteling.action;

import java.util.ArrayList;
import java.util.List;

// --------------------------------------------------------------------------------
// spriteling
// File: CompositeAction.java
// Author: markusmannel
// Date: 10-24-2018
//
// Copyright 2018 Markus Mannel
// --------------------------------------------------------------------------------
public class CompositeAction implements Action
{

    private List<Action> actions = new ArrayList<>(  );

    public void add(Action action)
    {
        actions.add( action );
    }


    @Override
    public void undo()
    {
        for( int i = actions.size() - 1; i >= 0; i-- )
        {
            actions.get( i ).undo();
        }

    }
}
