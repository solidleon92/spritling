package com.solidleon.spriteling.action;

import com.solidleon.spriteling.drawing.Sprite;

import java.awt.*;

// --------------------------------------------------------------------------------
// spriteling
// File: DrawAction.java
// Author: markusmannel
// Date: 10-24-2018
//
// Copyright 2018 Markus Mannel
// --------------------------------------------------------------------------------
public class DrawAction implements Action
{
    private final Sprite sprite;
    private final int x;
    private final int y;
    private final Color color;
    private final Color before;

    public DrawAction( Sprite sprite, int x, int y, Color color, Color before )
    {

        this.sprite = sprite;
        this.x = x;
        this.y = y;
        this.color = color;
        this.before = before;
    }

    @Override
    public void undo()
    {
        sprite.set( x, y, before );
    }
}
