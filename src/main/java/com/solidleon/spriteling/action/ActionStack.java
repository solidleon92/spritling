package com.solidleon.spriteling.action;

import java.util.ArrayList;
import java.util.List;

// --------------------------------------------------------------------------------
// spriteling
// File: ActionStack.java
// Author: markusmannel
// Date: 10-24-2018
//
// Copyright 2018 Markus Mannel
// --------------------------------------------------------------------------------
public class ActionStack
{
    private List<Action> actions = new ArrayList<>(  );


    public void push(Action action)
    {
        actions.add( action );
    }

    public void undoLast() {
        if (actions.size() > 0)
        {
            Action action = actions.remove( actions.size() - 1 );
            action.undo();
        }
    }

    public void clear()
    {
        actions.clear();
    }
}
