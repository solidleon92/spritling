package com.solidleon.spriteling.action;

// --------------------------------------------------------------------------------
// spriteling
// File: Action.java
// Author: markusmannel
// Date: 10-24-2018
//
// Copyright 2018 Markus Mannel
// --------------------------------------------------------------------------------
public interface Action
{
    void undo();
}
