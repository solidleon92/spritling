package com.solidleon.spriteling.ui;

import com.solidleon.spriteling.drawing.DrawSettings;
import com.solidleon.spriteling.drawing.Layer;
import com.solidleon.spriteling.drawing.Picture;

import javax.swing.*;

// --------------------------------------------------------------------------------
// spriteling
// File: LayerListModel.java
// Author: markusmannel
// Date: 10-24-2018
//
// Copyright 2018 Markus Mannel
// --------------------------------------------------------------------------------
public class LayerListModel extends AbstractListModel<Layer>
{

    private Picture picture;

    public LayerListModel( Picture picture )
    {
        this.picture = picture;
    }

    @Override
    public int getSize()
    {
        return picture.layers();
    }

    @Override
    public Layer getElementAt( int index )
    {
        return picture.getLayer( index );
    }

    public void refresh( DrawSettings drawSettings )
    {
        picture = drawSettings.picture;
        fireContentsChanged( this, 0, getSize() );
    }
}
