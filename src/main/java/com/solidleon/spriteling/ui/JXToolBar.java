package com.solidleon.spriteling.ui;

import javax.swing.*;

public class JXToolBar extends JToolBar {
    public JButton add(Action action) {
        JButton button = super.add(action);
        KeyStroke stroke = (KeyStroke) action.getValue(Action.ACCELERATOR_KEY);
        String actionCommandKey = (String) action.getValue(Action.ACTION_COMMAND_KEY);
        if (actionCommandKey == null)
            actionCommandKey = (String) action.getValue(Action.NAME);
        if (stroke != null) {
            // do the input/actionMap config here
            button.getActionMap().put(actionCommandKey, action);
            button.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
                    (KeyStroke) action.getValue(Action.ACCELERATOR_KEY), actionCommandKey);
        }
        return button;
    }
}
