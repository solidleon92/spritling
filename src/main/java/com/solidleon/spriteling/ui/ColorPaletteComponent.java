package com.solidleon.spriteling.ui;

import com.solidleon.spriteling.drawing.ColorPalette;
import com.solidleon.spriteling.drawing.DrawSettings;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

// --------------------------------------------------------------------------------
// spriteling
// File: ColorPaletteComponent.java
// Author: markusmannel
// Date: 10-24-2018
//
// Copyright 2018 Markus Mannel
// --------------------------------------------------------------------------------
public class ColorPaletteComponent extends JPanel
{

    public ColorPaletteComponent( DrawSettings drawSettings, ColorPalette palette )
    {
        super(new GridLayout( 0,  palette.colors.length) );

        setBorder( BorderFactory.createEmptyBorder( 2, 2, 2, 2 ) );

        for( int i = 0; i < palette.colors.length; i++ )
        {
            JLabel label = new JLabel(  );
            label.setPreferredSize( new Dimension( 16, 16 ) );
            label.setOpaque( true );
            label.setBackground( palette.colors[i] );
            add( label );

            label.addMouseListener( new MouseColorChange( drawSettings, palette, i, label ));
        }

    }

    class MouseColorChange extends MouseAdapter {
        private DrawSettings drawSettings;
        private ColorPalette palette;
        private int index;
        private JLabel label;

        public MouseColorChange( DrawSettings drawSettings, ColorPalette palette, int index, JLabel label )
        {
            this.drawSettings = drawSettings;
            this.palette = palette;
            this.index = index;
            this.label = label;
        }

        @Override
        public void mouseClicked( MouseEvent e )
        {
            if (e.getButton() == MouseEvent.BUTTON1)
                drawSettings.foreground = palette.colors[index];
            else if (e.getButton() == MouseEvent.BUTTON3)
            {
                Color col = JColorChooser.showDialog( ColorPaletteComponent.this, "Select palette color", palette.colors[index] );
                if (col != null)
                {
                    palette.colors[index] = col;
                    label.setBackground( col );
                    ColorPaletteComponent.this.repaint(  );
                }
            }
        }
    }
}
