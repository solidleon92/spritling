package com.solidleon.spriteling.ui;

import com.solidleon.spriteling.MainWindow;
import com.solidleon.spriteling.SimpleAction;
import com.solidleon.spriteling.drawing.ColorPalette;
import com.solidleon.spriteling.drawing.DrawSettings;

import javax.swing.*;
import java.awt.*;

// --------------------------------------------------------------------------------
// spriteling
// File: ColorPaletteComponent.java
// Author: markusmannel
// Date: 10-24-2018
//
// Copyright 2018 Markus Mannel
// --------------------------------------------------------------------------------
public class ColorManagerComponent extends JPanel
{


    private final JPanel palettes = new JPanel( new GridLayout( 0, 1 ) );

    public ColorManagerComponent( MainWindow mainWindow, DrawSettings drawSettings )
    {
        super( new BorderLayout() );

        setBorder( BorderFactory.createTitledBorder( "Color Palette" ) );

        JToolBar paletteTools = new JToolBar();
        paletteTools.setFloatable( false );
        paletteTools.add( new SimpleAction( "+", () ->
        {
            Color color = JColorChooser.showDialog( ColorManagerComponent.this, "Start Color", drawSettings.foreground );
            if (color != null)
            {
                palettes.add( new ColorPaletteComponent( drawSettings, new ColorPalette( color ) ) );
                palettes.revalidate();
                palettes.repaint();

                revalidate();
            }
        } ) );
        paletteTools.add( new SimpleAction( "-", null ) );
        add( paletteTools, BorderLayout.NORTH );

        palettes.add( new ColorPaletteComponent( drawSettings, new ColorPalette( Color.black ) ) );
        add( new JScrollPane( palettes ) );
    }
}
