package com.solidleon.spriteling.ui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.function.Consumer;
import java.util.function.Supplier;

// --------------------------------------------------------------------------------
// spriteling
// File: ColorChangeButton.java
// Author: markusmannel
// Date: 10-24-2018
//
// Copyright 2018 Markus Mannel
// --------------------------------------------------------------------------------
public class ColorChangeButton extends JButton
{

    private final Supplier<Color> selectedColor;
    private final Consumer<Color> onSelected;

    public ColorChangeButton( String text, Supplier<Color> selectedColor, Consumer<Color> onSelected )
    {
        super( new ImageIcon( ColorChangeButton.class.getResource( "/icons/color.png" ) ) );
        this.selectedColor = selectedColor;
        this.onSelected = onSelected;
        addActionListener( this::action );
    }

    private void action( ActionEvent e )
    {

        Color col = JColorChooser.showDialog( (Component) e.getSource(), "Foreground Color", selectedColor.get() );

        if( col != null )
        {
            onSelected.accept( col );
        }
    }
}
